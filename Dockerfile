FROM python:3.8-buster as builder

WORKDIR /app

COPY requirements.txt /app
RUN pip3 install -r requirements.txt

FROM python:3.8-slim-buster as runner

WORKDIR /app

COPY --from=builder /usr/local/lib/python3.8/site-packages /usr/local/lib/python3.8/site-packages

RUN apt update \
  && apt install -y libpq5 libxml2 \
  && apt-get clean \
  && rm -rf /var/lib/apt/lists/*

COPY . .

EXPOSE 8093

CMD ["python", "report.py"]
