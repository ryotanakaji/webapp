
from report import app

def test_get():
    app.config['TESTING'] = True
    client = app.test_client()
    result = client.get('/')
    assert result.status == "200 OK"
