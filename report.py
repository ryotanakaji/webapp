from flask import Flask, render_template, request, redirect, url_for
import os
import pickle
import flask
from os.path import join, dirname, realpath
import pandas as pd
from pandas_profiling import ProfileReport

app = Flask(__name__)
UPLOAD_FOLDER = './static/files'
app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER


@app.route('/')
def index():
    return render_template('index.html')

@app.route('/report')
def report():
     return app.send_static_file('report.html')

@app.route("/", methods=['POST'])
def uploadFiles():
      uploaded_file = request.files['file']
      if uploaded_file.filename != '':
           file_path = os.path.join(app.config['UPLOAD_FOLDER'], uploaded_file.filename)
           uploaded_file.save(file_path)

           with open(file_path, 'rb') as web:
               dict = pickle.load(web)
           df = pd.DataFrame(dict)

           report = ProfileReport(df)
           report.to_file("./static/report.html")

           os.remove(file_path)

      return redirect(url_for('report'))

if (__name__ == "__main__"):
     app.run(host='0.0.0.0', port=8093)
     # app.run()
